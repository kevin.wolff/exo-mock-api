import mockRequest from './MockServiceApi.js'

mockRequest('/login', {
    email: 'magle@live.fr',
    password: 'password'
}).then(res => console.log(res))

mockRequest('/sendMessage', {
    text: 'fzoi fzùpf zezz',
    timestamp: 1621088320,
    recipient_id: 3
}).then(res => console.log(res))

mockRequest('/newMessage').then(res => console.log(res))

mockRequest('/loadNewMessages', {
    id_new_messages: 3
}).then(res => console.log(res))
