// Créer des mock pour les endpoints suivant:

const mockRequest = (uri, params) => {
    switch (uri) {
        case uri = '/login':
            return login(params)
        case uri = '/sendMessage':
            return sendMessage(params)
        case uri = '/newMessage':
            return newMessage(params)
        case uri = '/loadNewMessages':
            return loadNewMessages(params)
    }
}

const promise = data => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            return resolve(data)
        }, 1000)
    })
}

/*
  @endpoint: /login
  @method: POST
  @params: { email: <text>, password:<text> }
  @response: { success: <boolean>, user: {name: <text>, avatar_url:<string>} }
*/
const login = params => {
    return promise({
        success: true,
        user: {
            name: 'Magle',
            avartar_url: 'https://monimage.com/id/1'
        }
    })
}

/*
  @endpoint: /sendMessage
  @method: POST
  @params: { text: <text>, timestamp:<number>, recipient_id:<number> }
  @response: { success: <boolean>, delivered: <boolean>} }
*/
const sendMessage = params => {
    return promise({
        success: true,
        delivered: true
    })
}

/*
  @endpoint: /newMessage
  @method: GET
  @params: {}
  @response: { success: <boolean>, from: <number>} }
*/
const newMessage = params => {
    return promise({
        success: true,
        from: 3
    })
}

/*
  @endpoint: /loadNewMessages
  @method: POST
  @params: { id_new_messages: <number> }
  @response: { success: <boolean>, {nb_message: <number>, messages: [{timestamp:<number>, body:<text> }] }} }
*/
const loadNewMessages = params => {
    return promise({
        success: true,
        data: {
            nb_message: 3,
            message: [{
                timestamp: 1621088320,
                body: 'fzen fzp âdfnj zeijfsp dzaadç!'
            }]
        }
    })
}

export default mockRequest